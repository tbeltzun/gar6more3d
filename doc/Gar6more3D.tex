\documentclass[11pt,a4]{article}
\usepackage{a4wide}
%\usepackage{pstcol}
\usepackage{pstricks,pst-node,pst-text,pst-3d} 
\usepackage{comm}
\usepackage[latin1]{inputenc}
\title{Documentation for Gar6more3D}
\author{Julien Diaz$^{1,2}$, Abdelaaziz Ezziani$^{2,1}$ and
  Nicolas Le Goff$^1$}
\begin{document}
\maketitle
\footnotetext[1]{EPI Magique-3D, Centre de Recherche
  Inria Bordeaux Sud-Ouest}
\footnotetext[2]{Laboratoire de Math\'ematiques et de
  leurs Applications, CNRS UMR-5142, Universit\'e de Pau et des Pays de
  l'Adour --  B\^atiment IPRA, avenue de l'Universit\'e  -- BP 1155-64013 PAU CEDEX}
This code compute the quasi-analytical solution of
several wave equation in two layered media, using
the Cagniard de Hoop
method~\cite{Cag,DH,VDH,PG,QG,JD,RAP_DE6509,Rap2,Rap4}. It produces
seismograms at given points.

The equations can be written in the general form.  
\begin{eqnarray}
 A(y)\derp {^2 U}{t^2}-B(z) U=\delta({\bf x}-{\bf x_s})\,f(t), 
\quad x\in\setR,\,y\in\setR\,z\in\setR
\end{eqnarray}
where $A$ and $B$ are operators satisfying\\[10pt]
\begin{tabular}{l}
$A(z)=A^+, \, B(z)=B^+, \quad z>0$, \\
$A(z)=A^-, \, B(z)=B^-, \quad z<0.$
\end{tabular} 
\\[10pt]
The code analytically compute the Green  function $u$ of the problem
\begin{eqnarray}
 A(z)\derp {^2 u}{t^2}-B(z) u=\delta({\mathbf
   x}-\mathbf{x_s})\,\delta(t), \quad x\in\setR, y\in\setR, z\in\setR
\end{eqnarray}
and convolves it with the source function $f$. You
can modify this function in the subroutine {\it
  lib/libgeneral/source.F90}. The convolution is
done by a numerical integration, that is why the
solution is only ``quasi-analytical''. You can
improve the accuracy of the solution by increasing
the number of intervals used for the integration
(the variable {\it Nint} in the data file {\it
  cdh3d.dat}). Moreover, in 3D, Cagniard-de Hoop
requires the computation of another integral
(see~\cite{Rap2,Rap4}), the number of intervals
for the numerical integration is given by {\it
  Nqint}
\section{Acoustic/acoustic}
The code computes a seismogram at points
$(x_i,y_i,z)_{i=1,Nx,j=1,Ny}$ of the pressure solution of the equations
\begin{eqnarray*}
 \derp {^2 P^+}{t^2}-{c^+}^2 \Delta P^+=\delta({\mathbf x}-\mathbf{x_s})f(t),\quad x\in\setR, z>0\\[10pt]
 \derp {^2 P^-}{t^2}-{c^-}^2 \Delta P^-=\delta({\mathbf x}-\mathbf{x_s})f(t),\quad x\in\setR, z<0
\end{eqnarray*}
We consider $\mathbf{x_s}=(0,h)$ and we use the transmission conditions :\\[10pt]
\begin{tabular}{|l} 
$P^+=P^-$, \\[10pt]
$\dsp \rho^+ \frac{\partial P^-}{\partial z}=\rho^- \frac{\partial P^+}{\partial z} .$
\end{tabular}\\
on the interface $z=0$.
The code also computes the displacement given by the relation~:
$$ \derp{\mathbf U^\pm}  t =-\frac{1}{\rho^\pm}{\mathbf \nabla} P^\pm.$$ 
Actually, for some reasons related to Cagniard-de
Hoop in $3D$, the code does not really compute the
pressure and the velocity, but the time-integral
of the pressure and the displacement. Therefore,
to obtain the actual pressure or the velocity, you
have to replace $f(t)$ by the source function you
are using.

\section{Acoustic/elastodynamic(isotropic)}
The code computes a seismogram at point
$(x_i,y_j)_{i=1,Nx,j=1,Ny}$ of the pressure (in the fluid) and the
velocity (in the solid) solution of the equations
\begin{eqnarray}
 \derp {^2 P^+}{t^2}-{c^+}^2 \Delta P^+=\delta({\mathbf x}-\mathbf{x_s})f(t),\quad x\in\setR, z>0,\\[10pt]
 \derp {^2 \mathbf{V}^-}{t^2}-(\lambda^-+2\mu^-)
 {\mathbf \nabla} (\nabla \cdot {\mathbf{V}})+\mu^-\nabla\times(\nabla \times {\mathbf{V}^-})=0,\quad x\in\setR, z<0,
\end{eqnarray}
with  $\mathbf{x_s}=(0,h)$ and the transmission conditions\\[10pt]
\begin{tabular}{|l} 
$\dsp \derp {V^-_z}t=-\frac{1}{\rho^+}\derp {P^+}{z},\quad y=0$, \\[10pt]
$\dsp (\lambda^-+2\mu^-) \derp {V_z^-}{z}+\lambda^- \derp{V_x^-}{x}=\derp{P^+}{t}$\\[10pt]
$\dsp \derp{V_x^-}z+\derp {V_z^-} x=0.$
\end{tabular}\\[10pt]
on the interface $y=0$. 
The code  also computes the velocity in the fluid by using the relation
$$\derp {V^+} t =-\frac{1}{\rho^+}\nabla P^+.$$ 

Actually, as for the acoustic/acoustic case, the
code does not really compute the pressure and the velocity, but the
time-integral of the pressure and the displacement.
 Therefore, to obtain the actual
pressure or the velocity, you have to replace
$f(t)$ by the source function you are using. 
\section{Acoustic/poroelastic (see~\cite{Rap2})}
The code computes  a seismogram at point
$(x_i,y_i,z)_{i=1,Nx,j=1,Ny}$ of the pressure
$P$, the displacement $U^+$ (in the fluid) and the
solid displacement $U_s^-$ (in the poroelastic medium) solution of the equations
$$ \begin{array}{lr}
\left|
 \begin{array}{l}
 \dsp \derp {^2 P^+}{t^2}-{c^+}^2 \Delta
 P^+=\delta({\mathbf
   x}-\mathbf{x_s})f(t),\\[10pt]
\dsp \derp {U^+} t =-\nabla P^+.
\end{array}
\right.
\end{array}$$
for $x\in\setR, z>0$ and 
$$\begin{array}{lr}
\left|
  \begin{array}{l}
\dsp  (1-\phi^-)\rho_s^- \derp {^2
  \mathbf U_s^-}{t^2}+\phi\rho_f^-\derp {^2
  \mathbf U_f^-
}{t^2}-(\lambda^-+2\mu^-) \nabla (\nabla \cdot
\mathbf U_s^-)+\mu^-\nabla\times(\nabla \times \mathbf U_s^-)+\beta\,\nabla P^-=0
,\\[10pt]
    \dsp  (1-a^-)\rho_f^- \derp {^2
  \mathbf U_s^-}{t^2}+a^-\rho_f^-\derp {^2
  \mathbf U_f^-}{t^2}+\nabla P^-=0\\[10pt]
\dsp \frac 1 {m^-} P^- +(\beta^--\phi^-) \nabla \cdot \mathbf U_s^-+ \phi^- \nabla \cdot \mathbf U_f^-=0
  \end{array}
\right.
 \end{array}$$
for $x\in\setR, z<0$, either with the open pore transmission
conditions (if parameter open is set to 1)\\[10pt] 
\begin{tabular}{|l} 
$\dsp \phi^-(U_{fz}^--U_{sz}^-)=U^+_z-U_{sz}^-,$\\[12pt]
$\dsp P^-=\rho^+\derp{P^+}{t},$\\[12pt]
$\dsp \big(\lambda^-+m^-\beta^-(\beta^--\phi^-)\big)\,\nabla\cdot \mathbf U_s^-+2\mu^-\derp{U_{sz}^-}{z}+m^-\beta^-\phi^-\nabla\cdot \mathbf U_f^-
=-\rho^+\derp{P^+}{t},$\\[12pt]
$\dsp\derp{U_{sx}^-}{z}+\derp{U_{sz}^-}{x}=0 ,$\\[12pt]
$\dsp\derp{U_{sy}^-}{z}+\derp{U_{sz}^-}{y}=0$.
\end{tabular}
\\[10pt]
or with the sealed pore transmission
conditions (if parameter open is set to 0)\\[10pt]
\begin{tabular}{|l} 
$\dsp \phi^-(U_{fz}^--U_{sz}^-)=U^+_z-U_{sz}^-,$\\[12pt]
$U_{fy}^-=U_{sy}^-,$\\[12pt]
$\dsp \big(\lambda^-+m^-\beta^-(\beta^--\phi^-)\big)\,\nabla\cdot \mathbf U_s^-+2\mu^-\derp{U_{sz}^-}{z}+m^-\beta^-\phi^-\nabla\cdot \mathbf U_f^-
=-\rho^+\derp{P^+}{t},$\\[12pt]
$\dsp\derp{U_{sx}^-}{z}+\derp{U_{sz}^-}{x}=0 ,$\\[12pt]
$\dsp\derp{U_{sy}^-}{z}+\derp{U_{sz}^-}{y}=0$.
\end{tabular}
\\[10pt]
on the interface $z=0$.
The code
does not compute the fluid displacement and the
pressure in the poroelastic medium, but there is
no particular difficulty to do that. 
Once again the code does not really compute the pressure but its time-integral.Therefore, to obtain the actual
pressure or the velocity, you have to replace
$f(t)$ by the source function you are using. 

\section{Poroelastic/poroelastic~\cite{Rap4}}
The code computes a seismogram at point
$(x_i,y_j,z)_{i=1,Nx,j=1,Ny}$ of the solid displacement $U$
solution of the equations
$$ \begin{array}{lr}
\!\!\!\!\left|
  \begin{array}{l}
\dsp  (1-\phi^+)\rho_s^+ \derp {^2
  \mathbf U_s^+}{t^2}+\phi^+\rho_f^+\derp {^2
  \mathbf U_f^+}{t^2}-\!(\lambda^++2\mu^+) \nabla (\nabla \cdot
\mathbf U_s^+)\!+\!\mu^+\nabla\times(\nabla \times \mathbf U_s^+)+\beta^+\nabla P^+\!=\!\nabla\delta({\mathbf x}-\mathbf{x_s}) F_s(t)
,\\[10pt]
    \dsp  (1-a^+)\rho_f^+ \derp {^2
  \mathbf U_s^+}{t^2}+a^+\rho_f^+\derp {^2
  \mathbf U_f^+}{t^2}+\nabla P^+=\nabla\delta({\mathbf x}-\mathbf{x_s}) F_s(t)\\[10pt]
\dsp \frac 1 {m^+} P^+ +(\beta^+-\phi^+) \nabla
\cdot \mathbf U_s^++ \phi^+ \nabla \cdot \mathbf U_f^+=\delta({\mathbf x}-\mathbf{x_s}) F_p(t)
  \end{array}
\right. 
\end{array}$$
for $x\in\setR, z>0$ and
$$ \begin{array}{lr}
\!\!\!\!\left|
  \begin{array}{l}
\dsp  (1-\phi^-)\rho_s^- \derp {^2
  \mathbf U_s^-}{t^2}+\phi^-\rho_f^-\derp {^2
  \mathbf U_f^-}{t^2}-(\lambda^-+2\mu^-) \nabla (\nabla \cdot
\mathbf U_s^-)+\mu^-\nabla\times(\nabla \times \mathbf U_s^-)+\beta^-\nabla P^-=0
,\\[10pt]
    \dsp  (1-a^-)\rho_f^- \derp {^2
  \mathbf U_s^-}{t^2}+a^-\rho_f^-\derp {^2
  \mathbf U_f^-}{t^2}+\nabla P^-=0\\[10pt]
\dsp \frac 1 {m^-} P^- +(\beta^--\phi^-) \nabla
\cdot \mathbf U_s^-+ \phi^- \nabla \cdot \mathbf U_f^-=0
  \end{array}
\right. 
\end{array}$$
for $x\in\setR, z<0$, with the transmission conditions on the interface $z=0$ \\[10pt]
\begin{tabular}{|l}
$\phi^-\,(U_{fz}^--U_{sz}^-)=\phi^+\,(U_{fz}^+-U_{sz}^+),$\\[12pt]
$\dsp \alpha^-\nabla\cdot \mathbf U_s^-
+2\mu^-\derp{ U_{sz}^-}{z}+m^-\beta^-\phi^-\,\nabla\cdot \mathbf U_f^-=
\alpha^+\nabla\cdot \mathbf U_s^+
+2\mu^+\derp{U_{sz}^+}{z} + m^+\beta^+\phi^+\,\nabla\cdot \mathbf U_f^+,$\\[12pt]
$\dsp \mu^-(\derp {U_{sx}^-}{z}+\derp{U_{sz}}{x}^-)=
\mu^+(\derp{ U_{sx}^+}{z} + \derp {U_{sz}^+}{x}),$\\[12pt]
$\dsp \mu^-(\derp {U_{sy}^-}{z}+\derp{U_{sz}}{y}^-)=
\mu^+(\derp{ U_{sy}^+}{z} + \derp {U_{sz}^+}{y}),$\\[12pt]
$U_{sx}^-=U_{sx}^+,\quad U_{sy}^-=U_{sy}^+,\quad U_{sz}^-=U_{sz}^+\quad P^-=P^+$
\end{tabular} 
\\[10pt]
with 
$$
\alpha^\pm=\lambda^\pm+m^\pm\beta^\pm(\beta^\pm-\phi^\pm).
$$
Actually the code computes the solution for each
source $F_s$ and $F_p$ separately. If you want a
bulk source ($F_s$), set the parameter
$\it type\_source$ to 1, if you want a pressure source
($F_p$), set the parameter
$\it type\_source$ to 2. 
\bibliographystyle{plain}
\bibliography{biblio} 
\end{document}
