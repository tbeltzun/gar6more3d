!! Copyright INRIA. Contributors : Julien DIAZ, Abdelaaziz EZZIANI 
!! and Nicolas LEGOFF 
!! 
!! Julien.Diaz@inria.fr, Abdelaaziz.Ezziani@univ-pau.fr 
!! and Nicolas.Le_goff@inria.fr
!! 
!! This software is a computer program whose purpose is to
!! compute the analytical solution of problems of waves propagation in two 
!! layered media such as
!! - acoustic/acoustic
!! - acoustic/elastodynamic
!! - acoustic/porous
!! - porous/porous,
!! based on the Cagniard-de Hoop method.
!! 
!! This software is governed by the CeCILL license under French law and
!! abiding by the rules of distribution of free software.  You can  use, 
!! modify and/ or redistribute the software under the terms of the CeCILL
!! license as circulated by CEA, CNRS and INRIA at the following URL
!! "http://www.cecill.info". 
!! 
!! As a counterpart to the access to the source code and  rights to copy,
!! modify and redistribute granted by the license, users are provided only
!! with a limited warranty  and the software's author,  the holder of the
!! economic rights,  and the successive licensors  have only  limited
!! liability. 
!! 
!! In this respect, the user's attention is drawn to the risks associated
!! with loading,  using,  modifying and/or developing or reproducing the
!! software by the user in light of its specific status of free software,
!! that may mean  that it is complicated to manipulate,  and  that  also
!! therefore means  that it is reserved for developers  and  experienced
!! professionals having in-depth computer knowledge. Users are therefore
!! encouraged to load and test the software's suitability as regards their
!! requirements in conditions enabling the security of their systems and/or 
!! data to be ensured and,  more generally, to use and operate it in the 
!! same conditions as regards security. 
!! 
!! The fact that you are presently reading this means that you have had
!! knowledge of the CeCILL license and that you accept its terms.
!! ========================================================================

  interface

!... Renvoit source(t) ...................................
     function source(t)
  Use m_source
  Use m_const
  implicit none
  real*8,intent(in) :: t
  real*8 :: source
  real*8 :: alpha,t1,t0
     end function source 

function arrivaltime(h,x,y,V1,V2)
implicit none
real*8,intent(in) ::h,x,y,V1,V2
real*8 :: arrivaltime,alpha,Rwork(8)
complex*16 :: Mat(4,4),Eig(4),Work(100)
integer :: LWork,Info
end function arrivaltime
function path(x,y,h,V1,V2,t)
  Use m_const
implicit none
real*8,intent(in) ::h,x,y,V1,V2,t
real*8 :: alpha,delta,test(4), Mat(4,4),Work(136),WR(4),WI(4)
real*8:: VS(4,4)
complex*16 ::Eig(4),path
integer ::Info,ii,Sdim
logical Bwork(4)
end function path
function derivee(x,y,h,V1,V2,pp)
  Use m_const
implicit none
real*8,intent(in) ::h,x,y,V1,V2
complex*16,intent(in)  :: pp
complex*16 :: derivee
end function derivee
function calccoeff_ap(pp,q)
  Use m_const
  Use m_phys
implicit none
complex*16,intent(in) :: pp
real*8,intent(in) ::q
integer ::Info,IPIV(4)
complex*16 :: k1,kf1,ks2,kpsi2
complex*16 :: Mat(4,4),coeff(4),calccoeff_ap(4)
real*8 :: X(2)
end function calccoeff_ap


function calccoeffP_ee(pp,q)
  Use m_const
  Use m_phys
implicit none
complex*16,intent(in) :: pp
real*8,intent(in) ::q
integer ::Info,IPIV(4)
complex*16 :: kp1,ks1,kp2,ks2
complex*16 :: Mat(4,4),coeff(4),calccoeffP_ee(4)
end function calccoeffP_ee


function calccoeffS_ee(pp,q)
  Use m_const
  Use m_phys
implicit none
complex*16,intent(in) :: pp
real*8,intent(in) ::q
integer ::Info,IPIV(4)
complex*16 :: kp1,ks1,kp2,ks2
complex*16 :: Mat(4,4),coeff(4),calccoeffS_ee(4)
end function calccoeffS_ee

function calccoeffP_ea(pp,q)
  Use m_const
  Use m_phys
implicit none
complex*16,intent(in) :: pp
real*8,intent(in) ::q
integer ::Info,IPIV(3)
complex*16 :: kp1,ks1,kp2,ks2
complex*16 :: Mat(3,3),coeff(3),calccoeffP_ea(3)
end function calccoeffP_ea


function calccoeffS_ea(pp,q)
  Use m_const
  Use m_phys
implicit none
complex*16,intent(in) :: pp
real*8,intent(in) ::q
integer ::Info,IPIV(3)
complex*16 :: kp1,ks1,kp2,ks2
complex*16 :: Mat(3,3),coeff(3),calccoeffS_ea(3)
end function calccoeffS_ea


function calccoefff_pp(pp,q)
  Use m_const
  Use m_phys
implicit none
complex*16,intent(in) :: pp
real*8,intent(in) ::q
integer ::Info,IPIV(6)
complex*16 :: kf1,ks1,kpsi1,kf2,ks2,kpsi2, X(2)
complex*16 :: Mat(6,6),coeff(6),calccoefff_pp(6)
end function calccoefff_pp
function calccoeffs_pp(pp,q)
  Use m_const
  Use m_phys
implicit none
complex*16,intent(in) :: pp
real*8,intent(in) ::q
integer ::Info,IPIV(6)
complex*16 :: kf1,ks1,kpsi1,kf2,ks2,kpsi2, X(2)
complex*16 :: Mat(6,6),coeff(6),calccoeffs_pp(6)
end function calccoeffs_pp
function calcq02(q02,x,z,h,V1,V2,Nqint,t,t0)
  Use m_const
implicit none
#include "m_inter2.h90"
real*8,intent(in) :: h,x,z,V1,V2,t,t0,q02
integer,intent(in) ::Nqint
integer :: nrec,test1
real*8 :: calcq02,dq,qlim,V1q,V2q,q,tq
end function calcq02

function calccoefff_wall_pp(pp,q)
  Use m_const
  Use m_phys
implicit none
complex*16,intent(in) :: pp
real*8,intent(in) ::q
integer ::Info,IPIV(6)
complex*16 :: kf1,ks1,kpsi1
complex*16 :: Mat(3,3),coeff(3),calccoefff_wall_pp(3),X(2)
end function calccoefff_wall_pp

function calccoefff_free_pp(pp,q)
  Use m_const
  Use m_phys
implicit none
complex*16,intent(in) :: pp
real*8,intent(in) ::q
integer ::Info,IPIV(3)
complex*16 :: kf1,ks1,kpsi1
complex*16 :: Mat(3,3),coeff(3),calccoefff_free_pp(3),X(2)

end function calccoefff_free_pp

function calccoeffs_free_pp(pp,q)
  Use m_const
  Use m_phys
implicit none
complex*16,intent(in) :: pp
real*8,intent(in) ::q
integer ::Info,IPIV(6)
complex*16 :: kf1,ks1,kpsi1,X(2)
complex*16 :: Mat(3,3),coeff(3),calccoeffs_free_pp(3)
end function calccoeffs_free_pp


function calccoeffs_wall_pp(pp,q)
  Use m_const
  Use m_phys
implicit none
complex*16,intent(in) :: pp
real*8,intent(in) ::q
integer ::Info,IPIV(6)
complex*16 :: kf1,ks1,kpsi1,X(2)
complex*16 :: Mat(3,3),coeff(3),calccoeffs_wall_pp(3)
end function calccoeffs_wall_pp

end interface


