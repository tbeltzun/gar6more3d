!! Copyright INRIA. Contributors : Julien DIAZ, Abdelaaziz EZZIANI 
!! and Nicolas LEGOFF 
!! 
!! Julien.Diaz@inria.fr, Abdelaaziz.Ezziani@univ-pau.fr 
!! and Nicolas.Le_goff@inria.fr
!! 
!! This software is a computer program whose purpose is to
!! compute the analytical solution of problems of waves propagation in two 
!! layered media such as
!! - acoustic/acoustic
!! - acoustic/elastodynamic
!! - acoustic/porous
!! - porous/porous,
!! based on the Cagniard-de Hoop method.
!! 
!! This software is governed by the CeCILL license under French law and
!! abiding by the rules of distribution of free software.  You can  use, 
!! modify and/ or redistribute the software under the terms of the CeCILL
!! license as circulated by CEA, CNRS and INRIA at the following URL
!! "http://www.cecill.info". 
!! 
!! As a counterpart to the access to the source code and  rights to copy,
!! modify and redistribute granted by the license, users are provided only
!! with a limited warranty  and the software's author,  the holder of the
!! economic rights,  and the successive licensors  have only  limited
!! liability. 
!! 
!! In this respect, the user's attention is drawn to the risks associated
!! with loading,  using,  modifying and/or developing or reproducing the
!! software by the user in light of its specific status of free software,
!! that may mean  that it is complicated to manipulate,  and  that  also
!! therefore means  that it is reserved for developers  and  experienced
!! professionals having in-depth computer knowledge. Users are therefore
!! encouraged to load and test the software's suitability as regards their
!! requirements in conditions enabling the security of their systems and/or 
!! data to be ensured and,  more generally, to use and operate it in the 
!! same conditions as regards security. 
!! 
!! The fact that you are presently reading this means that you have had
!! knowledge of the CeCILL license and that you accept its terms.
!! ========================================================================

FUNCTION path(x,y,h,V1,V2,t)
  USE m_const
  IMPLICIT NONE
#include "m_inter.h90"
  REAL*8,INTENT(in) ::h,x,y,V1,V2,t
  REAL*8 :: alpha,delta,test(4), Mat(4,4),Work(136),WR(4),WI(4)
  REAL*8:: VS(4,4),coe
  COMPLEX*16 ::Eig(4),path
  INTEGER ::Info,ii,Sdim
  LOGICAL Bwork(4)
  Mat=0.D0
  IF (y.NE.0) THEN
     IF (x.NE.0d0) THEN
        alpha=(x**2+y**2+h**2)**2-4*h**2*y**2;
        coe=4.D0*t*x*(x**2+y**2+h**2)/(2*(y**2/V2**2*(x**2+y**2-h**2)+&
             &h**2/V1**2*(x**2+h**2-y**2)-t**2*(3*x**2+y**2+h**2)))
        Mat=0.D0
        Mat(1,1)=-coe*4*t*x*(x**2+y**2+h**2)/alpha
        Mat(1,2)=2*coe**2*(y**2/V2**2*(x**2+y**2-h**2)+h**2/V1**2*(x**2+h**2-y**2)-t**2*(3*x**2+y**2+h**2))/alpha
        Mat(1,3)=4*coe**3*t*x*(y**2/V2**2+h**2/V1**2-t**2)/alpha
        Mat(1,4)=-coe**4*((y**2/V2**2-h**2/V1**2)**2+t**2*(t**2-2*y**2/V2**2-2*h**2/V1**2))/alpha 
        Mat(2,1)=1.D0
        Mat(3,2)=1.D0
        Mat(4,3)=1.D0
        CALL DGEES( 'N', 'N', 0, 4, Mat, 4, SDIM, WR, WI,&
             &VS, 4, WORK, 136, BWORK, INFO )
        EIG=-WI+ima*WR
        eig=eig/coe
        test=ABS(-y*SQRT(1/V2**2+eig**2)+h*SQRT(1/V1**2+eig**2)+ima*x*eig-t)
        ii=MINLOC(test,dim=1)
        path=eig(ii)
        IF(ABS(REAL(path)/AIMAG(path)).LT.1e-8) THEN
           IF (AIMAG(derivee(-ABS(x),y,h,V1,V2,path)).LT.0.D0) THEN
              test(ii)=100
              ii=MINLOC(test,dim=1)
              path=eig(ii)
           END IF
        END IF
        IF (REAL(path).LT.0.D0) THEN
           path=-REAL(path)+ima*AIMAG(path)
        END IF
     ELSE IF (ABS(y).NE.h) THEN
        alpha=(x**2+y**2+h**2)**2-4*h**2*y**2; 
        Mat(1,1)=2*(y**2/V2**2*(x**2+y**2-h**2)+h**2/V1**2*(x**2+h**2-y**2)-t**2*\
        (3*x**2+y**2+h**2))/alpha 
        Mat(1,2)=((y**2/V2**2-h**2/V1**2)**2+t**2*(t**2-2*y**2/V2**2-2*h**2/V1**\
2       ))/alpha
        delta=Mat(1,1)**2-4*Mat(1,2)
        IF (delta<0) THEN
           Eig(1)=SQRT((-Mat(1,1)+ima*SQRT(-delta))/2.)
           Eig(2)=-SQRT((-Mat(1,1)+ima*SQRT(-delta))/2.)
           Eig(3)=SQRT((-Mat(1,1)-ima*SQRT(-delta))/2.)
           Eig(4)=-SQRT((-Mat(1,1)-ima*SQRT(-delta))/2.)
        ELSE
           Eig(1)=SQRT((-Mat(1,1)+SQRT(delta))/2.+0*ima)
           Eig(2)=-SQRT((-Mat(1,1)+SQRT(delta))/2.+0*ima)
           Eig(3)=SQRT((-Mat(1,1)-SQRT(delta))/2.+0*ima)
           Eig(4)=-SQRT((-Mat(1,1)-SQRT(delta))/2.+0*ima)
        END IF
        test=ABS(-y*SQRT(1/V2**2+eig**2)+h*SQRT(1/V1**2+eig**2)+ima*x*eig-t) 
        ii=MINLOC(test,dim=1) 
        path=eig(ii) 
        IF(ABS(REAL(path)/AIMAG(path)).LT.1e-8) THEN 
           IF (AIMAG(derivee(-ABS(x),y,h,V1,V2,path)).LT.0.D0) THEN 
              test(ii)=100 
              ii=MINLOC(test,dim=1) 
              path=eig(ii) 
           END IF
        END IF
        IF (REAL(path).LT.0.D0) THEN 
           path=-REAL(path)+ima*AIMAG(path) 
        END IF

     ELSE
        path=SQRT(t**2*(t**2-2*h**2/V2**2-2*h**2/V1**2)/(4*t**2*h**2)+0*ima)
     END IF
  ELSE
     delta=h**2*t**2-h**4/V1**2-x**2*h**2/V1**2;
     IF (delta<0) THEN
        IF(x>0) THEN
           path=-ima*x*t/(h**2+x**2)+ima*SQRT(-delta)/(h**2+x**2);
        ELSE
           path=-ima*x*t/(h**2+x**2)-ima*SQRT(-delta)/(h**2+x**2);
        END IF
     ELSE
        path=-ima*x*t/(h**2+x**2)+SQRT(delta)/(h**2+x**2);
     END IF
  END IF
END FUNCTION path
