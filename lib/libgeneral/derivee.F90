!! Copyright INRIA. Contributors : Julien DIAZ, Abdelaaziz EZZIANI 
!! and Nicolas LEGOFF 
!! 
!! Julien.Diaz@inria.fr, Abdelaaziz.Ezziani@univ-pau.fr 
!! and Nicolas.Le_goff@inria.fr
!! 
!! This software is a computer program whose purpose is to
!! compute the analytical solution of problems of waves propagation in two 
!! layered media such as
!! - acoustic/acoustic
!! - acoustic/elastodynamic
!! - acoustic/porous
!! - porous/porous,
!! based on the Cagniard-de Hoop method.
!! 
!! This software is governed by the CeCILL license under French law and
!! abiding by the rules of distribution of free software.  You can  use, 
!! modify and/ or redistribute the software under the terms of the CeCILL
!! license as circulated by CEA, CNRS and INRIA at the following URL
!! "http://www.cecill.info". 
!! 
!! As a counterpart to the access to the source code and  rights to copy,
!! modify and redistribute granted by the license, users are provided only
!! with a limited warranty  and the software's author,  the holder of the
!! economic rights,  and the successive licensors  have only  limited
!! liability. 
!! 
!! In this respect, the user's attention is drawn to the risks associated
!! with loading,  using,  modifying and/or developing or reproducing the
!! software by the user in light of its specific status of free software,
!! that may mean  that it is complicated to manipulate,  and  that  also
!! therefore means  that it is reserved for developers  and  experienced
!! professionals having in-depth computer knowledge. Users are therefore
!! encouraged to load and test the software's suitability as regards their
!! requirements in conditions enabling the security of their systems and/or 
!! data to be ensured and,  more generally, to use and operate it in the 
!! same conditions as regards security. 
!! 
!! The fact that you are presently reading this means that you have had
!! knowledge of the CeCILL license and that you accept its terms.
!! ========================================================================

!!$function derivee(x,y,h,V1,V2,pp)
!!$  Use m_const
!!$implicit none
!!$real*8,intent(in) ::h,x,y,V1,V2
!!$complex*16,intent(in)  :: pp
!!$complex*16 :: derivee
!!$if (abs(imagpart(pp)).eq.1/V1**2) then
!!$   derivee=0;
!!$else
!!$       derivee=-y*pp/sqrt(1/V2**2+pp**2)+h*pp/sqrt(1/V1**2+pp**2)+ima*x;
!!$    if (derivee.ne.0.d0) then
!!$      derivee=1./derivee;
!!$    else
!!$       derivee=0;
!!$    end if
!!$ end if
!!$end function derivee
FUNCTION derivee(x,y,h,V1,V2,pp)
  USE m_const
IMPLICIT NONE
REAL*8,INTENT(in) ::h,x,y,V1,V2
COMPLEX*16,INTENT(in)  :: pp
COMPLEX*16 :: derivee
IF (ABS(aimag(pp)).EQ.1/V1**2) THEN
   derivee=0;
ELSEIF (ABS(aimag(pp)).EQ.1/V2**2) THEN
derivee=0;
ELSEIF (ABS(-y*pp/SQRT(1/V2**2+pp**2)+h*pp/SQRT(1/V1**2+pp**2)+ima*x).LT.ABS(x)*1e-5) THEN
derivee=0;
ELSE
       derivee=1/(-y*pp/SQRT(1/V2**2+pp**2)+h*pp/SQRT(1/V1**2+pp**2)+ima*x);
 END IF
END FUNCTION derivee
