!! Copyright INRIA. Contributors : Julien DIAZ, Abdelaaziz EZZIANI 
!! and Nicolas LEGOFF 
!! 
!! Julien.Diaz@inria.fr, Abdelaaziz.Ezziani@univ-pau.fr 
!! and Nicolas.Le_goff@inria.fr
!! 
!! This software is a computer program whose purpose is to
!! compute the analytical solution of problems of waves propagation in two 
!! layered media such as
!! - acoustic/acoustic
!! - acoustic/elastodynamic
!! - acoustic/porous
!! - porous/porous,
!! based on the Cagniard-de Hoop method.
!! 
!! This software is governed by the CeCILL license under French law and
!! abiding by the rules of distribution of free software.  You can  use, 
!! modify and/ or redistribute the software under the terms of the CeCILL
!! license as circulated by CEA, CNRS and INRIA at the following URL
!! "http://www.cecill.info". 
!! 
!! As a counterpart to the access to the source code and  rights to copy,
!! modify and redistribute granted by the license, users are provided only
!! with a limited warranty  and the software's author,  the holder of the
!! economic rights,  and the successive licensors  have only  limited
!! liability. 
!! 
!! In this respect, the user's attention is drawn to the risks associated
!! with loading,  using,  modifying and/or developing or reproducing the
!! software by the user in light of its specific status of free software,
!! that may mean  that it is complicated to manipulate,  and  that  also
!! therefore means  that it is reserved for developers  and  experienced
!! professionals having in-depth computer knowledge. Users are therefore
!! encouraged to load and test the software's suitability as regards their
!! requirements in conditions enabling the security of their systems and/or 
!! data to be ensured and,  more generally, to use and operate it in the 
!! same conditions as regards security. 
!! 
!! The fact that you are presently reading this means that you have had
!! knowledge of the CeCILL license and that you accept its terms.
!! ========================================================================

SUBROUTINE sub_transmit_aa(Nt_loc,J_array_loc)
  USE m_phys  
  USE m_num
  USE m_source
  USE m_sismo
  USE m_const
  IMPLICIT NONE 
#include "m_interface.h90"
  INTEGER I,J,K,L
  REAL*8 :: t0,x,y,ddt,src,t,tau,F,q02,V1q,V2q,dq,q,uxinter,uzinter,pinter,uxinterbis,uzinterbis,pinterbis,tau0
  COMPLEX*16::pp,Coeff,k1, k2

  INTEGER :: Nt_loc
  INTEGER, DIMENSION(Nt) :: J_array_loc
  INTEGER :: J_loc

  F=amp/V1**2
  DO I=1,Nx
     x=X1+(I-1)*dx
     y=Y1+(I-1)*dy
     x=SQRT(x**2+y**2)
     t0=arrivaltime(h,x,z,V1,V2)
     DO J_loc=1,Nt_loc
       J = J_array_loc(J_loc)
       t=T1+(j-1)*dt
       IF (t.GT.t0) THEN
          IF (t-2.*tdelay.GT.t0) THEN
             ddt=2*tdelay/Nint
             tau0=t-2.*tdelay
          ELSE
             ddt=(t-t0)/Nint
             tau0=t0
          END IF
         uxinter=0.;uzinter=0.;pinter=0.
         q02=0
          DO k=1,Nint
             tau=tau0+(k-1)*ddt+ddt/2.
             src=source(t-tau)
             q02=calcq02(q02,x,z,h,V1,V2,Nq02,tau,t0)
             dq=q02/Nqint
             q=dq/2.
             uxinterbis=0.;uzinterbis=0.;pinterbis=0. 
              DO l=1,Nqint
                V1q=V1/SQRT(1+V1**2*q**2)
                V2q=V2/SQRT(1+V2**2*q**2)
                pp=path(-ABS(x),z,h,V1q,V2q,tau)
                k1=SQRT(pp**2+1/V1q**2)
                k2=SQRT(pp**2+1/V2q**2)
                Coeff=rho2/(rho2*k1+rho1*k2)
                Coeff=Coeff*derivee(-ABS(x),z,h,V1q,V2q,pp)
                uxinterbis=uxinterbis+real(ima*pp*Coeff)
                uzinterbis=uzinterbis-real(k2*Coeff)
                pinterbis=pinterbis+real(Coeff)
                q=q+dq
             END DO
             uxinter=uxinter+uxinterbis*dq*src
             uzinter=uzinter+uzinterbis*dq*src
             pinter=pinter+pinterbis*dq*src
          END DO
          Ux(i,j)=Ux(i,j)+F*uxinter/(rho2*pi**2)*ddt
          Uz(i,j)=Uz(i,j)+F*uzinter/(rho2*pi**2)*ddt
          P(i,j)=P(i,j)+F*pinter/pi**2*ddt
       END IF
     END DO
  END DO
END SUBROUTINE sub_transmit_aa
