!! Copyright INRIA. Contributors : Julien DIAZ, Abdelaaziz EZZIANI 
!! and Nicolas LEGOFF 
!! 
!! Julien.Diaz@inria.fr, Abdelaaziz.Ezziani@univ-pau.fr 
!! and Nicolas.Le_goff@inria.fr
!! 
!! This software is a computer program whose purpose is to
!! compute the analytical solution of problems of waves propagation in two 
!! layered media such as
!! - acoustic/acoustic
!! - acoustic/elastodynamic
!! - acoustic/porous
!! - porous/porous,
!! based on the Cagniard-de Hoop method.
!! 
!! This software is governed by the CeCILL license under French law and
!! abiding by the rules of distribution of free software.  You can  use, 
!! modify and/ or redistribute the software under the terms of the CeCILL
!! license as circulated by CEA, CNRS and INRIA at the following URL
!! "http://www.cecill.info". 
!! 
!! As a counterpart to the access to the source code and  rights to copy,
!! modify and redistribute granted by the license, users are provided only
!! with a limited warranty  and the software's author,  the holder of the
!! economic rights,  and the successive licensors  have only  limited
!! liability. 
!! 
!! In this respect, the user's attention is drawn to the risks associated
!! with loading,  using,  modifying and/or developing or reproducing the
!! software by the user in light of its specific status of free software,
!! that may mean  that it is complicated to manipulate,  and  that  also
!! therefore means  that it is reserved for developers  and  experienced
!! professionals having in-depth computer knowledge. Users are therefore
!! encouraged to load and test the software's suitability as regards their
!! requirements in conditions enabling the security of their systems and/or 
!! data to be ensured and,  more generally, to use and operate it in the 
!! same conditions as regards security. 
!! 
!! The fact that you are presently reading this means that you have had
!! knowledge of the CeCILL license and that you accept its terms.
!! ========================================================================

SUBROUTINE elastoacous(Nt_loc,J_array_loc,myrank)
  USE m_phys  
  USE m_num
  USE m_source
  USE m_sismo
  IMPLICIT NONE 

  INTEGER :: Nt_loc,myrank
  INTEGER, DIMENSION(Nt) :: J_array_loc

!!! Computation of the incident wave
  ALLOCATE(Ux(1:nx,1:nt))
  ALLOCATE(Uy(1:nx,1:nt))
  ALLOCATE(Uz(1:nx,1:nt))
  Ux=0.d0
  Uy=0.d0
  Uz=0.d0
  IF (z.GE.0d0) THEN
     IF(ampP.NE.0D0) THEN
        IF (myrank.EQ.0) THEN
           WRITE(6,*) 'Computation of the incident P wave'
        END IF
        CALL sub_incidP_ea(Nt_loc,J_array_loc)
        IF (myrank.EQ.0) THEN
           WRITE(6,*) 'Computation of the reflected PP wave'
        END IF
        CALL sub_reflexPP_ea(Nt_loc,J_array_loc)
        IF (myrank.EQ.0) THEN
           WRITE(6,*) 'Computation of the reflected PS wave'
        END IF
        CALL sub_reflexPS_ea(Nt_loc,J_array_loc)
     END IF
     IF(ampS.NE.0D0) THEN
        IF (myrank.EQ.0) THEN
           WRITE(6,*) 'Computation of the incident S wave'
        END IF
        CALL sub_incidS_ea(Nt_loc,J_array_loc)
        IF (myrank.EQ.0) THEN
           WRITE(6,*) 'Computation of the reflected SS wave'
        END IF
        CALL sub_reflexSS_ea(Nt_loc,J_array_loc)
        IF (myrank.EQ.0) THEN
           WRITE(6,*) 'Computation of the reflected SP wave'
        END IF
        CALL sub_reflexSP_ea(Nt_loc,J_array_loc)
     END IF
  ELSE
     IF(ampP.NE.0D0) THEN
        IF (myrank.EQ.0) THEN
           WRITE(6,*) 'Computation of the transmitted Pf wave'
        END IF
        CALL sub_transmitPf_ea(Nt_loc,J_array_loc)
     END IF
     IF(ampS.NE.0D0) THEN
        IF (myrank.EQ.0) THEN
           WRITE(6,*) 'Computation of the transmitted Sf wave'
        END IF
        CALL sub_transmitSf_ea(Nt_loc,J_array_loc)
     END IF
  END IF
END SUBROUTINE elastoacous
