!! Copyright INRIA. Contributors : Julien DIAZ, Abdelaaziz EZZIANI 
!! and Nicolas LEGOFF 
!! 
!! Julien.Diaz@inria.fr, Abdelaaziz.Ezziani@univ-pau.fr 
!! and Nicolas.Le_goff@inria.fr
!! 
!! This software is a computer program whose purpose is to
!! compute the analytical solution of problems of waves propagation in two 
!! layered media such as
!! - acoustic/acoustic
!! - acoustic/elastodynamic
!! - acoustic/porous
!! - porous/porous,
!! based on the Cagniard-de Hoop method.
!! 
!! This software is governed by the CeCILL license under French law and
!! abiding by the rules of distribution of free software.  You can  use, 
!! modify and/ or redistribute the software under the terms of the CeCILL
!! license as circulated by CEA, CNRS and INRIA at the following URL
!! "http://www.cecill.info". 
!! 
!! As a counterpart to the access to the source code and  rights to copy,
!! modify and redistribute granted by the license, users are provided only
!! with a limited warranty  and the software's author,  the holder of the
!! economic rights,  and the successive licensors  have only  limited
!! liability. 
!! 
!! In this respect, the user's attention is drawn to the risks associated
!! with loading,  using,  modifying and/or developing or reproducing the
!! software by the user in light of its specific status of free software,
!! that may mean  that it is complicated to manipulate,  and  that  also
!! therefore means  that it is reserved for developers  and  experienced
!! professionals having in-depth computer knowledge. Users are therefore
!! encouraged to load and test the software's suitability as regards their
!! requirements in conditions enabling the security of their systems and/or 
!! data to be ensured and,  more generally, to use and operate it in the 
!! same conditions as regards security. 
!! 
!! The fact that you are presently reading this means that you have had
!! knowledge of the CeCILL license and that you accept its terms.
!! ========================================================================

PROGRAM Gar6more3D

  use mpi

  USE m_phys  
  USE m_num
  USE m_source
  USE m_sismo
  USE m_const
  IMPLICIT NONE


  INTEGER I,J
  REAL*8 delta,A1inv(2,2),A2inv(2,2),Fstar(2),Mataux(2,2),P1inv(2,2)
  REAL*8 kb1,ks1,kf1,kb2,ks2,kf2,x,y,cost,sint

  ! MPI variables
  INTEGER :: myrank, nproc
  INTEGER :: ier
  INTEGER, DIMENSION(MPI_STATUS_SIZE) :: status

  ! dispatch variables
  INTEGER, DIMENSION(:), ALLOCATABLE :: J_array_loc
  INTEGER :: Nt_loc
  ier=0
  myrank=0
  nproc=0
  ! MPI initialization
  CALL MPI_INIT(ier)

  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,nproc,ier)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,myrank,ier)
  WRITE(6,*) nproc,myrank,ier,MPI_COMM_WORLD 


  pi=2.d0*dasin(1.D0)
  ima=(0,1)
  OPEN(20,FILE="Gar6more3D.dat")
  IF(myrank.EQ.0) THEN
     WRITE(6,*) 'Do you want'
     WRITE(6,*) '- An infinite homogeneous medium (1);'
     WRITE(6,*) '- A semi-infinite homogeneous media with a free surface boundary condition at it bottom (2):'
     WRITE(6,*) '- A semi-infinite homogeneous media with a wall boundary condition at it bottom (3):'
     WRITE(6,*) '- A bilayered infinite media with a plane interface (4):'
  END IF
  READ(20,*) type_medium
  IF(myrank.EQ.0) THEN
     WRITE(6,*) type_medium
  END IF
  IF(type_medium.EQ.4) THEN
     IF(myrank.EQ.0) THEN
        WRITE(6,*) 'Is the first medium acoustic (0), elastodynamic (1) or porous (2)?'
     END IF
     READ(20,*) type_medium_1
     IF(myrank.EQ.0) THEN
        WRITE(6,*) type_medium_1
     END IF
  ELSE
     IF(myrank.EQ.0) THEN
        WRITE(6,*) 'Is the first medium acoustic (0), elastodynamic (1) or porous (2)?'
     END IF
     READ(20,*) type_medium_1
     IF(myrank.EQ.0) THEN
        WRITE(6,*) type_medium_1
     END IF
  END IF
  IF(myrank.EQ.0) THEN
     WRITE(6,*) 'Source time derivative (2 for Ricker)?'
  END IF
  READ(20,*) deriv
  IF(myrank.EQ.0) THEN
     WRITE(6,*) deriv
  END IF
  IF(myrank.EQ.0) THEN
     WRITE(6,*) 'Frequency of the source?'
  END IF
  READ(20,*) f0
  IF(myrank.EQ.0) THEN
     WRITE(6,*) f0
  END IF
  IF(type_medium_1.EQ.0) THEN
     IF(myrank.EQ.0) THEN
        WRITE(6,*) 'Amplitude of the source?'
     END IF
     READ(20,*) amp
     IF(myrank.EQ.0) THEN
        WRITE(6,*) amp
     END IF
  ELSEIF (type_medium_1.EQ.1) THEN
     IF(myrank.EQ.0) THEN
        WRITE(6,*) 'Amplitude of the P source?'
     END IF
     READ(20,*) ampP
     IF(myrank.EQ.0) THEN
        WRITE(6,*) ampP
        WRITE(6,*) 'Amplitude of the S source?'
     END IF
     READ(20,*) ampS
     IF(myrank.EQ.0) THEN
        WRITE(6,*) ampS
     END IF
  ELSEIF(type_medium_1.EQ.2) THEN
     IF(myrank.EQ.0) THEN
        WRITE(6,*) 'Amplitude of the bulk source (fu)?'
     END IF
     READ(20,*) ampbulk
     IF(myrank.EQ.0) THEN
        WRITE(6,*) ampbulk
        WRITE(6,*) 'Amplitude of the pressure source (fp)?'
     END IF
     READ(20,*) ampP
     IF(myrank.EQ.0) THEN
        WRITE(6,*) ampP
     END IF
  END IF
  IF(myrank.EQ.0) THEN
     WRITE(6,*) 'delay of the source (f(t)=0 if t>2tdelay)?' 
  ENDIF
  READ(20,*) tdelay 
  IF(myrank.EQ.0) THEN
     WRITE(6,*) tdelay 
     WRITE(6,*) 'Height of the source?'
  END IF
  READ(20,*) h
  IF(myrank.EQ.0) THEN
     WRITE(6,*) h    
     WRITE(6,*) 'Height of the line of receivers?'
  END IF
  READ(20,*) Z
  IF(myrank.EQ.0) THEN
     WRITE(6,*) Z
     WRITE(6,*) 'Abscissa of the first receiver?'
  END IF
  READ(20,*) X1
  IF(myrank.EQ.0) THEN
     WRITE(6,*) X1
     WRITE(6,*) 'Ordinate of the first receiver?'
  END IF
  READ(20,*) Y1
  IF(myrank.EQ.0) THEN
     WRITE(6,*) Y1
     WRITE(6,*) 'Abscissa of the last receiver?'
  END IF
  READ(20,*) X2
  IF(myrank.EQ.0) THEN
     WRITE(6,*) X2
     WRITE(6,*) 'Ordinate of the last receiver?'
  END IF
  READ(20,*) Y2
  IF(myrank.EQ.0) THEN
     WRITE(6,*) Y2
     WRITE(6,*) 'How many receivers?'
  END IF
  READ(20,*) NX
  IF(myrank.EQ.0) THEN
     WRITE(6,*) NX
  END IF
  IF (nx.GE.2) THEN
     dx=(X2-X1)/(Nx-1)
     dy=(Y2-Y1)/(Nx-1)
  ELSE
     dx=0
     dy=0
  END IF
  IF(myrank.EQ.0) THEN
     WRITE(6,*) 'dx=',dx
     WRITE(6,*) 'dy=',dy
     WRITE(6,*) 'At which time shall the seismo begin?'
  END IF
  READ(20,*) T1
  IF(myrank.EQ.0) THEN
     WRITE(6,*) T1
     WRITE(6,*) 'At which time shall the seismo stop?'
  END IF
  READ(20,*) T2
  IF(myrank.EQ.0) THEN
     WRITE(6,*) T2
     WRITE(6,*) 'What is the time-step?'
  END IF
  READ(20,*) dt
  IF(myrank.EQ.0) THEN
     WRITE(6,*) dt
  END IF
  Nt=AINT((T2-T1)/dt)+1

  IF(myrank.EQ.0) THEN
     WRITE(6,*) 'How many intervals are required for the numerical computation of the time convolution?'
  END IF
  READ(20,*) Nint
  IF(myrank.EQ.0) THEN
     WRITE(6,*) Nint
     WRITE(6,*) 'How many intervals are required for the numerical computation of the space integration?'
  END IF
  READ(20,*) Nqint
  IF(myrank.EQ.0) THEN
     WRITE(6,*) Nqint
  END IF
  Nq02=MAX(10,Nqint/10)
  IF (type_medium_1.EQ.0) THEN
     IF(myrank.EQ.0) THEN
        WRITE(6,*) 'mu and rho?'
     END IF
     READ(20,*) mu1,rho1
     IF(myrank.EQ.0) THEN
        WRITE(6,*) mu1,rho1
     END IF
     V1=dsqrt(mu1/rho1)
     IF(myrank.EQ.0) THEN         
        WRITE(6,*) 'V1=', V1
     END IF
  ELSEIF  (type_medium_1.EQ.1) THEN
     IF(myrank.EQ.0) THEN
        WRITE(6,*) 'mu,lambda,rho'
     END IF
     READ(20,*) mu1,lambda1,rho1
     IF(myrank.EQ.0) THEN
        WRITE(6,*) mu1,lambda1,rho1
     END IF
     Vp1=dsqrt((lambda1+2*mu1)/rho1)
     Vs1=dsqrt(mu1/rho1)
     IF(myrank.EQ.0) THEN
        WRITE(6,*) 'Vp1, Vs1',Vp1, Vs1
     END IF
  ELSEIF  (type_medium_1.EQ.2) THEN
     IF(myrank.EQ.0) THEN
        WRITE(6,*) 'kb (Frame bulk modulus), ks (Solid bulk modulus),&
             & kf (Fluid bulk modulus), mu (Frame shear modulus),&
             & rhof (Fluid density), rhos (Solid density), &
             &phi (Porosity), a(Tortuosity)?' 
     END IF
     READ(20,*) kb1,ks1,kf1,mu1,rhof1,rhos1,phi1,a1  
     IF(myrank.EQ.0) THEN
        WRITE(6,*) kb1,ks1,kf1,mu1,rhof1,rhos1,phi1,a1  
     END IF
     lambda1=kb1-2*mu1/3;
     rho1=phi1*rhof1+(1-phi1)*rhos1;
     rhow1=a1*rhof1/phi1;
     beta1=1-kb1/ks1;
     M1=1/(phi1/kf1+(beta1-phi1)/ks1);
     la1=lambda1+M1*(beta1-phi1)**2;
     R1=M1*phi1**2;
     ga1=M1*phi1*(beta1-phi1);
     S2=la1+2*mu1;
     AA1(1,1)=rho1 
     AA1(1,2)=rhof1
     AA1(2,1)=rhof1
     AA1(2,2)=rhow1;
     A1inv(1,1)=AA1(2,2)
     A1inv(1,2)=-AA1(1,2)
     A1inv(2,1)=-AA1(2,1)
     A1inv(2,2)=AA1(1,1)
     A1inv=A1inv/(AA1(1,1)*AA1(2,2)-AA1(1,2)**2)
     B1(1,1)=lambda1+2*mu1+M1*beta1**2 
     B1(1,2)=M1*beta1
     B1(2,1)=M1*beta1 
     B1(2,2)=M1;
     TT1=MATMUL(A1inv,B1);
     delta=(TT1(1,1)+TT1(2,2))**2+4*(TT1(1,2)*TT1(2,1)-TT1(1,1)*TT1(2,2))
     D1(1)=((TT1(1,1)+TT1(2,2))+SQRT(Delta))/2.
     D1(2)=((TT1(1,1)+TT1(2,2))-SQRT(Delta))/2.
     IF (TT1(2,1).EQ.0.d0) THEN
        IF (D1(1).EQ.TT1(1,1)) THEN
           P1(1,1)=1.;P1(2,1)=0.;
           IF(TT1(1,2).EQ.0.d0) THEN
              P1(1,2)=0.;P1(2,2)=1.;
           ELSE
              P1(1,2)=-TT1(1,2)/(TT1(1,1)-D1(2));P1(2,2)=1.;
           END IF
        ELSE
           P1(1,2)=1.;P1(2,2)=0.
           IF(TT1(1,2).EQ.0.d0) THEN
              P1(1,1)=0.;P1(2,1)=1.;
           ELSE
              P1(1,1)=-TT1(1,2)/(TT1(1,1)-D1(1));P1(2,1)=1.;
           END IF
        END IF
     ELSE
        P1(1,1)=-(TT1(2,2)-D1(1))/TT1(2,1);P1(2,1)=1.
        P1(1,2)=-(TT1(2,2)-D1(2))/TT1(2,1);P1(2,2)=1.
     END IF
     P1(:,1)=P1(:,1)/SQRT(SUM(P1(:,1)**2))
     P1(:,2)=P1(:,2)/SQRT(SUM(P1(:,2)**2))
     Vpsi1=SQRT(mu1*rhow1/(rho1*rhow1-rhof1**2))
     Vf1=SQRT(D1(1))
     Vs1=SQRT(D1(2))
     IF(myrank.EQ.0) THEN
        WRITE(6,*) 'Vpsi1, Vf1, Vs1',Vpsi1,Vf1,Vs1
        WRITE(6,*) 'P1'
        WRITE(6,*) P1(1,1),P1(1,2)
        WRITE(6,*) P1(2,1),P1(2,2)
     END IF
     Fstar(1)=ampbulk-M1*beta1*ampP
     Fstar(2)=ampbulk-M1*ampP
     P1inv(1,1)=P1(2,2)
     P1inv(1,2)=-P1(1,2)
     P1inv(2,1)=-P1(2,1)
     P1inv(2,2)=P1(1,1)
     P1inv=P1inv/(P1(1,1)*P1(2,2)-P1(1,2)*P1(2,1))
     Mataux=MATMUL(P1inv,A1inv)
     Fstar=MATMUL(Mataux,Fstar);
     F1=Fstar(1);
     F2=Fstar(2);
     IF(myrank.EQ.0) THEN
        WRITE(6,*) 'F1, F2', F1, F2
     END IF
  END IF
  IF (type_medium.EQ.4) THEN
     IF(myrank.EQ.0) THEN
        WRITE(6,*) 'Is the second medium acoustic (0), elastodynamic (1) or porous (2)?'
     END IF
     READ(20,*) type_medium_2
     IF(myrank.EQ.0) THEN
        WRITE(6,*) type_medium_2
     END IF
     SELECT CASE(type_medium_2)
     CASE(0)
        IF(myrank.EQ.0) THEN
           WRITE(6,*) 'mu and rho?'
        END IF
        READ(20,*) mu2,rho2
        IF(myrank.EQ.0) THEN
           WRITE(6,*) mu2,rho2
        END IF
        V2=dsqrt(mu2/rho2)
        IF(myrank.EQ.0) THEN
           WRITE(6,*) 'V2=', V2
        END IF
     CASE(1)
        IF(myrank.EQ.0) THEN
           WRITE(6,*) 'mu,lambda,rho'
        END IF
        READ(20,*) mu2,lambda2,rho2
        IF(myrank.EQ.0) THEN
           WRITE(6,*) mu2,lambda2,rho2
        END IF
        Vp2=dsqrt((lambda2+2*mu2)/rho2)
        Vs2=dsqrt(mu2/rho2)
        IF(myrank.EQ.0) THEN
           WRITE(6,*) 'Vp2, Vs2',Vp2, Vs2
        END IF
     CASE(2)
        IF(myrank.EQ.0) THEN
           WRITE(6,*) ' kb (Frame bulk modulus), ks (Solid bulk modulus), kf (Fluid bulk modulus), mu (Frame shear modulus),&
                & rhof (Fluid density), rhos (Solid density), phi (Porosity), a(Tortuosity)?' 
        END IF
        READ(20,*) kb2,ks2,kf2,mu2,rhof2,rhos2,phi2,a2  
        IF(myrank.EQ.0) THEN
           WRITE(6,*) kb2,ks2,kf2,mu2,rhof2,rhos2,phi2,a2  
        END IF
        lambda2=kb2-2*mu2/3;
        rho2=phi2*rhof2+(1-phi2)*rhos2;
        rhow2=a2*rhof2/phi2;
        beta2=1-kb2/ks2;
        M2=1/(phi2/kf2+(beta2-phi2)/ks2);
        la2=lambda2+M2*(beta2-phi2)**2;
        R2=M2*phi2**2;
        ga2=M2*phi2*(beta2-phi2);
        S2=la2+2*mu2;
        AA2(1,1)=rho2 
        AA2(1,2)=rhof2
        AA2(2,1)=rhof2
        AA2(2,2)=rhow2;
        B2(1,1)=lambda2+2*mu2+M2*beta2**2 
        B2(1,2)=M2*beta2
        B2(2,1)=M2*beta2 
        B2(2,2)=M2;
        A2inv(1,1)=AA2(2,2)
        A2inv(1,2)=-AA2(1,2)
        A2inv(2,1)=-AA2(2,1)
        A2inv(2,2)=AA2(1,1)
        A2inv=A2inv/(AA2(1,1)*AA2(2,2)-AA2(1,2)**2)
        TT2=MATMUL(A2inv,B2);
        delta=(TT2(1,1)+TT2(2,2))**2+4*(TT2(1,2)*TT2(2,1)-TT2(1,1)*TT2(2,2))
        D2(1)=((TT2(1,1)+TT2(2,2))+SQRT(Delta))/2.
        D2(2)=((TT2(1,1)+TT2(2,2))-SQRT(Delta))/2.
        IF (TT2(2,1).EQ.0.d0) THEN
           IF (D2(1).EQ.TT2(1,1)) THEN
              P2(1,1)=1.;P2(2,1)=0.;
              IF(TT2(1,2).EQ.0.d0) THEN
                 P2(1,2)=0.;P2(2,2)=1.;
              ELSE
                 P2(1,2)=-TT2(1,2)/(TT2(1,1)-D2(2));P2(2,2)=1.;
              END IF
           ELSE
              P2(1,2)=1.;P2(2,2)=0.
              IF(TT2(1,2).EQ.0.d0) THEN
                 P2(1,1)=0.;P2(2,1)=1.;
              ELSE
                 P2(1,1)=-TT2(1,2)/(TT2(1,1)-D2(1));P2(2,1)=1.;
              END IF
           END IF
        ELSE
           P2(1,1)=-(TT2(2,2)-D2(1))/TT2(2,1);P2(2,1)=1.
           P2(1,2)=-(TT2(2,2)-D2(2))/TT2(2,1);P2(2,2)=1.
        END IF
        P2(:,1)=P2(:,1)/SQRT(SUM(P2(:,1)**2))
        P2(:,2)=P2(:,2)/SQRT(SUM(P2(:,2)**2))
        Vpsi2=SQRT(mu2*rhow2/(rho2*rhow2-rhof2**2))
        Vf2=SQRT(D2(1))
        Vs2=SQRT(D2(2))
        IF(myrank.EQ.0) THEN
           WRITE(6,*) 'Vpsi2, Vf2, Vs2',Vpsi2,Vf2,Vs2
           WRITE(6,*) 'P2'
           WRITE(6,*) P2(1,1),P2(1,2)
           WRITE(6,*) P2(2,1),P2(2,2)
        END IF
        IF (type_medium_1.EQ.0) THEN
           IF(myrank.EQ.0) THEN
              WRITE(6,*) 'Do you want open pore (1) or sealed pore (2) conditions ?'
           END IF
           READ(20,*) OPEN
           IF(myrank.EQ.0) THEN
              WRITE(6,*) OPEN
           END IF
        END IF
     CASE DEFAULT
        WRITE(6,*) 'Not implemented'
        STOP
     END SELECT
  END IF
  ! dispatching the time steps among the processors
  IF (Nt < nproc) THEN
     WRITE(6,*) 'Nt should be greater than nproc!'
     CALL MPI_ABORT(MPI_COMM_WORLD,-1,ier)
  ENDIF

  ALLOCATE(J_array_loc(Nt))

  Nt_loc = 0
  DO J=1,Nt
     IF (MODULO(J-1,nproc) == myrank) THEN
        Nt_loc = Nt_loc+1
        J_array_loc(Nt_loc) = J
     ENDIF
  ENDDO
  CLOSE(20)
  WRITE(6,*) 'PROC',myrank,'got',Nt_loc,'of', Nt, 'time steps.'
  IF (type_medium.NE.4) THEN 
     SELECT CASE(type_medium_1)
     CASE(0)
        CALL acousacous(Nt_loc,J_array_loc,myrank)
     CASE(1)
        CALL elastoelasto(Nt_loc,J_array_loc,myrank)
     CASE(2)
        CALL poroporo(Nt_loc,J_array_loc,myrank)
     CASE DEFAULT
        WRITE(6,*) 'Not implemented'
        STOP
     END SELECT
  ELSE
     SELECT CASE(type_medium_1)
     CASE(0)
        SELECT CASE(type_medium_2)
        CASE(0)
           CALL acousacous(Nt_loc,J_array_loc,myrank)
        CASE(1)
           CALL acouselasto(Nt_loc,J_array_loc,myrank)
        CASE(2)
           CALL acousporo(Nt_loc,J_array_loc,myrank)
        CASE  DEFAULT
           WRITE(6,*) 'Not implemented'
           STOP
        END SELECT
     CASE(1)
        SELECT CASE(type_medium_2)
        CASE(0)
           CALL elastoacous(Nt_loc,J_array_loc,myrank)
        CASE(1)
           CALL elastoelasto(Nt_loc,J_array_loc,myrank)
        CASE(2)
           WRITE(6,*) 'Not implemented'
           STOP
        CASE  DEFAULT
           WRITE(6,*) 'Not implemented'
           STOP
        END SELECT
     CASE(2)        
        SELECT CASE(type_medium_2)
        CASE(0)
           WRITE(6,*) 'Not implemented'
           STOP
        CASE(1)
           WRITE(6,*) 'Not implemented'
           STOP
        CASE(2)
           CALL poroporo(Nt_loc,J_array_loc,myrank)
        CASE  DEFAULT
           WRITE(6,*) 'Not implemented'
           STOP
        END SELECT
     CASE DEFAULT
        WRITE(6,*) 'Not implemented'
        STOP
     END SELECT
  END IF
  DO I=1,Nx
     x=X1+(I-1)*dx
     y=Y1+(I-1)*dy
     IF (x**2+y**2.NE.0.) THEN
        cost=x/SQRT(x**2+y**2)
        sint=y/SQRT(x**2+y**2)
        Uy(i,:)=-sint*Ux(i,:)
        Ux(i,:)=-cost*Ux(i,:)
     END IF
  END DO

  ! assembling the results
  IF (myrank == 0) THEN
     DO J = 1, Nt
        IF (MODULO(J-1,nproc) /= myrank) THEN
           CALL MPI_RECV(Ux(1,J),nx,MPI_DOUBLE_PRECISION,MODULO(J-1,nproc),J,MPI_COMM_WORLD,status,ier)
           CALL MPI_RECV(Uy(1,J),nx,MPI_DOUBLE_PRECISION,MODULO(J-1,nproc),J,MPI_COMM_WORLD,status,ier)
           CALL MPI_RECV(Uz(1,J),nx,MPI_DOUBLE_PRECISION,MODULO(J-1,nproc),J,MPI_COMM_WORLD,status,ier)
           IF (type_medium_1.EQ.0) THEN
              CALL MPI_RECV(P(1,J),nx,MPI_DOUBLE_PRECISION,MODULO(J-1,nproc),J,MPI_COMM_WORLD,status,ier)
           ENDIF
        ENDIF
     ENDDO
  ELSE
     DO J = 1, Nt
        IF (MODULO(J-1,nproc) == myrank) THEN
           CALL MPI_SEND(Ux(1,J),nx,MPI_DOUBLE_PRECISION,0,J,MPI_COMM_WORLD,ier)
           CALL MPI_SEND(Uy(1,J),nx,MPI_DOUBLE_PRECISION,0,J,MPI_COMM_WORLD,ier)
           CALL MPI_SEND(Uz(1,J),nx,MPI_DOUBLE_PRECISION,0,J,MPI_COMM_WORLD,ier)
           IF (type_medium_1.EQ.0) THEN
              CALL MPI_SEND(P(1,J),nx,MPI_DOUBLE_PRECISION,0,J,MPI_COMM_WORLD,ier)
           ENDIF
        ENDIF
     ENDDO
  ENDIF

  IF (myrank == 0) THEN
     IF(type_medium_1.EQ.0) THEN
        OPEN(20,FILE='Ux.dat')
        OPEN(21,FILE='Uy.dat')
        OPEN(22,FILE='Uz.dat')
        OPEN(23,FILE='P.dat')
        DO J=1,Nt
           WRITE(20,*) T1+(j-1)*dt,(Ux(i,j),i=1,Nx)
           WRITE(21,*) T1+(j-1)*dt,(Uy(i,j),i=1,Nx)
           WRITE(22,*) T1+(j-1)*dt,(Uz(i,j),i=1,Nx)
           WRITE(23,*) T1+(j-1)*dt,(P(i,j),i=1,Nx)
        END DO
        CLOSE(20)
        CLOSE(21)
        CLOSE(22)
        CLOSE(23)
     ELSE
        OPEN(20,FILE='Ux.dat')
        OPEN(21,FILE='Uy.dat')
        OPEN(22,FILE='Uz.dat')
        DO J=1,Nt
           WRITE(20,*) T1+(j-1)*dt,(Ux(i,j),i=1,Nx)
           WRITE(21,*) T1+(j-1)*dt,(Uy(i,j),i=1,Nx)
           WRITE(22,*) T1+(j-1)*dt,(Uz(i,j),i=1,Nx)
        END DO
        CLOSE(20)
        CLOSE(21)
        CLOSE(22)
     END IF
  ENDIF

  CALL MPI_FINALIZE(ier)
END PROGRAM Gar6more3D
